# To install:
#
#  - unpack this in /usr/src or /usr/local/src or something - doesn't matter.
#  - do a 'make install' in the tetrix directory
#  - tetrix gets installed in /usr/local/bin
#  - this will create a high score file in /usr/tmp, so doing it again
#    later on will erase high scores for the machine.
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

# Where to purt the scores. This location reflects a bygone era of shared machines;
# you may want to change it to somewhere under your home directory. 
SCORE_FILE=/usr/tmp/.TetScores

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

CFLAGS=-O -DSCORE_FILE='"$(SCORE_FILE)"'
LFLAGS=-s
OBJS= MoveR.o MoveL.o DrawP.o AdvanceP.o Rotate.o Colors.o tet.o
INCS= tet.h

tetrix: $(OBJS) $(INCS)
	cc $(LFLAGS) $(OBJS) -lncurses -o tetrix

MoveR.o: MoveR.c tet.h
MoveL.o: MoveL.c tet.h
DrawP.o: DrawP.c tet.h
AdvanceP.o: AdvanceP.c tet.h
Rotate.o: Rotate.c tet.h
Colors.o: Colors.c tet.h
tet.o: tet.c tet.h

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .6

.adoc.6:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

install: tetrix.6 uninstall
	cp tetrix /usr/bin
	cp tetrix.6 /usr/share/man/man6/tetrix.6

uninstall:
	rm -f /usr/bin/install /usr/share/man/man6/tetrix.6

clean:
	rm -f tetrix *.o tetrix tetrix.6 tetrix-*.rpm tetrix-*.tar.gz index.html *~
	rm -f tetrix.html MANIFEST

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

cppcheck:
	cppcheck --quiet tet.h $(OBJS:.o=.c)

SOURCES = README COPYING NEWS.adoc control tetrix.adoc Makefile tet.h $(OBJS:.o=.c)

tetrix-$(VERS).tar.gz: $(SOURCES) tetrix.6
	@ls $(SOURCES) tetrix.6 | sed s:^:tetrix-$(VERS)/: >MANIFEST
	@(cd ..; ln -s tetrix tetrix-$(VERS))
	(cd ..; tar -czf tetrix/tetrix-$(VERS).tar.gz `cat tetrix/MANIFEST`)
	@(cd ..; rm tetrix-$(VERS))

dist: tetrix-$(VERS).tar.gz

version:
	@echo $(VERS)

release: tetrix-$(VERS).tar.gz tetrix.html
	shipper version=$(VERS) | sh -e -x

refresh: tetrix.html
	shipper -N -w version=$(VERS) | sh -e -x
